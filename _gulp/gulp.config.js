'use strict';

module.exports = {

  'browserPort'  : 3010,
  'UIPort'       : 3011,
  'serverPort'   : 3012,
  'testPort'     : 3100,

  'build': {
    'html': 'build/{app}',
    'css': 'build/{app}/css',
    'js': 'build/{app}/js'
  },

  'apps': {
    'ui-library': {
      'html': {
        'src': ['./src/html/**/*.html'],
        'map': ['./src/html/components/**/*.html']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/ui-library.main.scss',
          './src/sass/apps/pwc.main.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': ['./src/js/jquery/pwc.script.js']
      }
    }
  }
};
