'use strict';

var config        = require('../gulp.config');
var handleErrors  = require('../util/handleErrors');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var directoryMap  = require('gulp-directory-map');
var browserSync   = require('browser-sync');

gulp.task('ui-library-map', ['default'], function(done) {
  var app = global.app,
    html_map = config.apps[app].html.map,
    html_dest = (config.build.html).replace('{app}', app),
    json_file = 'ui-library-map.json';

  if (app == 'ui-library') {
    return gulp.src(html_map)
      .pipe(directoryMap({
        filename: json_file
      }))
      .pipe(gulp.dest(html_dest))
      .on('error', handleErrors)
      .pipe(browserSync.stream({ once: true }))
      .on('end', function() {
        logger('Updated "' + json_file + '"');
      });
  } else {
    done();
  }
});

