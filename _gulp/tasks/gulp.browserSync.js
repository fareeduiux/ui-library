'use strict';

var config        = require('../gulp.config');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var browserSync   = require('browser-sync');

gulp.task('browserSync', ['default'], function() {
  var app = global.app;

  browserSync.init({
    server: (config.build.html).replace('{app}', app),
    port: config.browserPort,
    ui: {
      port: config.UIPort
    },
    ghostMode: {
      links: false
    }
  });
});

