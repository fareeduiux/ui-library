Welcome to UI LIBRARY 
This is the on going project to build the robust UI library which can compete in the world. 

## Overview
###Where HTML5 really rocks :)

One of the first kind of Fully Semantic UI Library with superb moduler workflow, which ensure the high re-usablity of components to expedite website rapid development and prototyping.


### HTML Tree
1. Overview (Create Folder)
	a. Library Intro (Create File)
2. Developers
	a. Intro
	b. layouts
	c. grid
	d. buttons
	e. buttons-group
	f. inputs
	g. checkboxes
	f. radio buttons
	h. themes (Colour schemes)
3. UX/Authors
	a. Intro (component's intro)
	b. App Headers
	c. App Footers
	d. Banners
	c. Call to Action (CTA)

## Your's Task to do
1. delete all the html files and folder inside html folder (except index.html)
2. now create the folder as per above html tree. i.e. currently you will create three folder 1. Overview, 2. Developers, 3. UI/Authors (components)
3. Then create files inside each folder as per above tree with same name mentions above.
4. Kick start development of all the possible layouts you can think of for website like row based i.e. pepper.com.au, 2 columns etc. etc. etc.
5. You have to sumbit (pull request) of layouts by end of this week COB Saturday 27-02-16

#### Good Luck /=_