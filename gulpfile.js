'use strict';

/*
 * gulpfile.js
 * ===========
 * Rather than manage one giant configuration file responsible
 * for creating multiple tasks, each task has been broken out into
 * its own file in _gulp/tasks. Any file in that folder gets automatically
 * required by the loop in ./_gulp/index.js (required below).
 *
 * To add a new task, simply add a new task file to gulp/tasks.
 */

var env = process.env.NODE_ENV || 'development';

global.isProduction = (env == 'production');

require('./_gulp/gulp.index');