var $ = require('./vendors/jquery'),
	//add your js module here;
    hamburgerMenu = require('./components/hamburger-menu'),
    loginMenu = require('./components/login-menu'),
    contentSearch = require('./components/content-search'),
    searchResults = require('./components/search-results'),
    pepperTestimonial = require('./components/pepper-testimonial'),
    carousels = require('./components/carousels'),
    preventDefault = require('./components/prevent-default'),
    uilib = require('./ui-library');

/*! Components Library | pepper.com.au */
uilib();
// enD: UI-LIB-SCRIPTS


// PROJECT SCRIPT
$(document).ready(function() {
  hamburgerMenu();
  loginMenu();
  contentSearch();
  searchResults();
  pepperTestimonial();
  carousels();
  preventDefault();
});
/*! End $(document).ready() */