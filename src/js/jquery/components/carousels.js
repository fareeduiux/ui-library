var $ = jQuery = require('../vendors/jquery'),
    slick = require('../vendors/slick');

module.exports = function() {
  // Carousel-container
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 400,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  // Carousel-circle
  $('.circle-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    centerMode: false,
    responsive: [{
      breakpoint: 1024,
      settings: "unslick"
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 400,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });

  // Carousel-gallery
  $('body .gallery-slider').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    centerMode: false,
    responsive: [{
      breakpoint: 960,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 400,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });

  //light box for gallery images
  $('body').on('click', '.lightbox-images>div>div>div', function() {
    //lighbox for tablet and desktop only.
    if ($(window).width() >= 768) {
      var imageSrc = $(this).find('article').css('background-image').slice('5').split('"')[0];
      var caption = $(this).find('article header').html();
      //alert(imageSrc);
      $('.light-box.image-box').fadeIn('fast', function() {
        $('.light-box.image-box>section img').attr('src', imageSrc);
        $('.light-box.image-box>section figcaption').html(caption);
        $('.light-box.image-box>section').slideDown('fast', function(){
          var maxHeight = ($('.light-box>section img').height() <= 750) ? $('.light-box.image-box>section img').height() : 750;
          $('.light-box.image-box>section').css('max-height',maxHeight + 'px');
        });
      });

      $('body').on('click', '.image-box a.close-bt', function(e) {
        e.preventDefault();
        $('.light-box.image-box>section').slideUp('fast', function() {
          $('.light-box.image-box').fadeOut('fast');
        })
      });
    }
  });
};