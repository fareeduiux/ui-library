var $ = jQuery = require('../vendors/jquery');

module.expors = function() {
  $('.light-box header a.close-popup, .light-box>section>.button-group>button.secondary').on('click', function (e) {
    e.preventDefault();
    $('.light-box>section').slideUp('fast', function () {
      $('.light-box').fadeOut('fast');
    })
  });

//campaign submit
  $('body').on('click', '#directCampaign button.submit', function (e) {
    e.preventDefault();
    if (Validate($('#directCampaign'))) {
      $('.light-box').fadeIn('fast', function () {
        $('.light-box>section').slideDown('fast');
      });
    }
  });

  $("body").on('click', ".light-box .button-group>button:not('.secondary')", function (e) {
    e.preventDefault();
    $('.light-box>section').slideUp('fast', function () {
      $('.light-box').fadeOut('fast', function () {
        $('.page-loader').show();
        $(window).scrollTop(0);
      });
    });

    //Analytics send before form submission CR: 05-08-15 -
    try {
      digitalData.events.push({
        "type": "analytics",
        "event": "campaignForm",
        "BuyAHomeOrRefinanceInTheNext12Months": $("input[name='BuyAHomeOrRefinanceInTheNext12Months']:checked").next().html(),
        "BuyACarInTheNext12Months": $("input[name='BuyACarInTheNext12Months']:checked").next().html(),
        "email": $('#email').val()
      });
    } catch (e) {
      //console.log(e.message);
      //regardless analytics passed or failed, finally it should submit the form.
    } finally {
      var formData = $('#directCampaign').serialize();
      var redirectTo = $('#directCampaign').attr('data-redirect');
      var url = "/campaign-on-the-house";
      $.ajax({
        method: "POST",
        url: url,
        data: formData
      })
        .done(function (data) {
          //////console.log("Data Saved: ", data);

          if (data.status === "success") {
            $('.page-loader').hide();
            $('#directCampaign').hide();
            window.location.href = redirectTo;
            //$('.pepper-form p').addClass('centre-block');
          } else {
            ////console.log('Form Submitted UNSUCCESSFULl');
            $('.page-loader').hide();
          }

        })
        .fail(function (data) {
          ////console.log('Unable to send the enquiry, please try again later');
          $('.page-loader').hide();
        });
    } //finaly of analytics
  });
};