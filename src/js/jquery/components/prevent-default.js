var $ = require('../vendors/jquery');

//all preventDefault behaviours like details summary collapse in chrome will prevent here
module.exports = function() {
    $('body').on('click', 'summary, details', function(e) {
        e.preventDefault();
    });
}