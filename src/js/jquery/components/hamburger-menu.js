var $ = require('../vendors/jquery');

module.exports = function() {
	$('body').on('click', '.level-one li>a.has-sub, .level-two li>a.has-sub', function(e) {
		e.preventDefault();
		$('.nav-login.active').trigger('click');
		var _submenu = $(this).next('ul').attr('class').split(' ')[0];
		ToggleMenu($(this), _submenu);
	});

	//window resize doesn't work with Jquery 'on' delegation.
	$(window).resize(function() {
		$('#HamburgerToggle').prop('checked', false);
	});

	$('body').on('click', '.nav-main', function(e){
		var isMenuOverlayClicked = (e.target.nodeName == 'MENU') ? true : false;
		if($('#HamburgerToggle').is(':checked') && isMenuOverlayClicked) {
			e.preventDefault();
			$('#HamburgerToggle').prop('checked', false);
		}
	});

	function ToggleMenu(el, submenu) {
		el.toggleClass('active').next('.' + submenu).toggleClass('active');
	}

	//reset sub menu on 2nd level no sub click;
	$('body').on('click', ".level-two li>a:not('.has-sub')", function(e) {
		$(this).blur();
		$('.level-two a.has-sub.active').removeClass('active');
		$('.level-three.active').removeClass('active');
	});
}
