var $ = require('../vendors/jquery');

module.exports = function() {
  $('body').on('click', '.pepper-testimonial figure>img', function(e) {
    e.preventDefault();
    if (!$(this).hasClass('active')) {
      var personQuote = $(this).data('quote');
      var personName = $(this).data('author');
      var personLink = $(this).data('href');
      var personLinkTxt = $(this).data('href-text');
      var blockQuote = $(this).parent().parent().find('blockquote');
      blockQuote.find('p').html(personQuote);
      blockQuote.find('small').html(personName);
      blockQuote.find('a').html(personLinkTxt).attr('href', personLink);
      $('.pepper-testimonial figure>img').css('opacity', '0.5').css('width', '35%');
      $(this).css('opacity', '1').css('width', '49%');
      $('.pepper-testimonial blockquote').toggleClass('second');
      $('.pepper-testimonial figure>img').removeClass('active');
      $(this).addClass('active');
    }
  });
};
