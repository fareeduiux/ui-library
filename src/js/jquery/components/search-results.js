var $ = require('../vendors/jquery');

module.exports = function() {
  if (window.location.href.indexOf('search-results') > 0) {
    setTimeout(function() {
      var qStr = getParameterByName('queryStr');
      var totalFound = $('.search-results>h2>em').html();
      try {
        digitalData.events.push({
          "type": "analytics",
          "event": "internalSearch",
          "searchTerm": qStr,
          "searchResultCount": totalFound
        });
      } catch (e) {}
      //SendAnalytics($('.search-box').find("a"));
    }, 3500);
  }
};
