// UI-LIB-SCRIPTS
var $ = require('./vendors/jquery'),
prism = require('./vendors/prism'),
slicks = require('./components/carousels.js');

module.exports = function() { 
  $.getJSON('/ui-library-map.json', function( data ) {
    var nav_items = [];
    $.each( data, function( key, val ) {
      var children = [];
      $.each(val, function(filename, filepath) {
        children.push( '<li><a href="/components/' + filepath + '">' + filename + '</li>' );
      });

      var children_html = $( "<ul/>", { html: children.join( "" ) });
      var parent = $("<li/>", { html: '<div>' + key + '</div>' });
      children_html.appendTo(parent);

      nav_items.push(parent.html());
    });

    $( "<ul/>", {
      "class": "ui-components",
      html: nav_items.join( "" )
    }).appendTo( "#ui-library-nav" ).bind();
  });

$(document).ready(function(){
    $('body').on('click', '.ui-components a',function(e){
        e.preventDefault();
        $('.ui-components a').removeClass('active');
        $(this).addClass('active');
      $.get($(this).attr('href'), function (data) {
                        $('#component-display').html($(data));
                        var clonecore = $('#component-display').clone();
                        clonecore.find('details').remove();
                        clonecore = clonecore.text(clonecore.html());
                        $('#html-code code').html(clonecore);
                        //$('#html-code code').html($(clonecore).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\t/g, '&nbsp;&nbsp;').replace(/\n/g, '<br/>'));

                        //.replace(/\r\n/g, '\n')
                        //.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
                        //update slick sliders after inject /// check carousel.js for slick components
                        slicks();
                        
                        Prism.plugins.NormalizeWhitespace.setDefaults({
  'remove-trailing': true,
  'remove-indent': true,
  'left-trim': true,
  'right-trim': true,
  'break-lines': 100,
  'indent': 0,
  'remove-initial-line-feed': true,
  'tabs-to-spaces': 1,
  'spaces-to-tabs': 0
}); //prism normalise
                        Prism.highlightAll();
                    }); //get file
    });
  });
}